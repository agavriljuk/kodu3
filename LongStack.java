import java.util.LinkedList;

import javax.imageio.IIOException;

public class LongStack {
	private LinkedList<Long> stack;

	public static void main(String[] argum) {
		// TODO!!! Your tests here!
		String s = "1 2 - 3 +";
		System.out.println(interpret(s));
	}

	LongStack() {
		// TODO!!! Your constructor here!
		stack = new LinkedList<Long>();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		LongStack clone = new LongStack();
		for (int i = 0; i < stack.size(); i++) {
			clone.push(stack.get(i));
		}
		return clone; // TODO!!! Your cod e here!
	}

	public boolean stEmpty() {
		try {
			stack.getFirst();
			return false;
		} catch (java.util.NoSuchElementException e) {
			return true;
		}
		// return false; // TODO!!! Your code here!
	}

	public void push(long a) {
		// TODO!!! Your code here!
		stack.addLast(a);
	}

	public long pop() {
		long retrn = stack.getLast();
		stack.removeLast();
		return retrn;
	} // pop

	public void op(String s) {
		long teine = pop();
		long esimene = pop();
		long result = 0;
		if (s.equals("+")) {
			result=teine + esimene;
			push(result);
		}
		if (s.equals("-")) {
			result=esimene - teine;
			push(result);
		}

		if (s.equals("*")) {
			result=esimene * teine;
			push(result);
		}
		
		if (s.equals("/")) {
			result=esimene / teine;
			push(result);
		}

	}

	public long tos() {
		return (stack.getLast());
	}

	@Override
	public boolean equals(Object other) {
		String first = this.toString();
		String second = other.toString();
		if (first.equals(second)) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		String s = stack.toString();
		return s;
	}

	public static long interpret(String s) {
		LongStack output = new LongStack();
		int op = 0;
		int arvud = 0;

		if (s.length() == 0)
			throw new RuntimeException("Incorrect input. String is null - " + s);

		String[] elemendid = s.trim().split("\\s+");
		

		try {
			Integer.parseInt(elemendid[0]);
		} catch (NumberFormatException exc) {
			throw new RuntimeException("Esimene element peab olla arv - " + s);
		}

		for (int i = 0; i < elemendid.length; i++) {

			try {
				output.push(Long.parseLong(elemendid[i]));
				arvud++;

			} catch (NumberFormatException e) {
				if (!elemendid[i].equals("+") && !elemendid[i].equals("-")
						&& !elemendid[i].equals("*")
						&& !elemendid[i].equals("/"))
					throw new RuntimeException("Input have incorrect symbols" + s);
				else if (output.stEmpty())
					throw new RuntimeException(
							"Esimene element peab olla arv "+ s);
				else if (output.toString().length() < 2)
					throw new RuntimeException(
							"Elementi peab olla rohkem "+ s);
				output.op(elemendid[i]);
				op++;
			}

		}
		if (arvud - 1 != op)
			throw new RuntimeException("String-s ei ole piisava arvu hulga selleks et korraldada nii palju operatsioone");
		return output.tos();

	}

}